FROM alpine:3.12
LABEL Nosebit Dev Team <dev@nosebit.com>

# Environment variables
ENV THRIFT_VERSION=0.11.0

# Install dev dependencies
RUN build_pkgs="git autoconf automake boost libtool flex bison" && \
    apk --update add \
        ${build_pkgs} \
        build-base \
        supervisor \
        make \
        bash \
        python3 \
        curl \
        nodejs \
        npm \
        lz4-dev \
        musl-dev \
        cyrus-sasl-dev \
        openssl-dev

# Install thrift
RUN cd /tmp && \
    git clone -b ${THRIFT_VERSION} https://github.com/apache/thrift.git && \
    cd thrift && \
    ./bootstrap.sh && \
    ./configure && \
    make && \
    make install

# Remove build packages
RUN apk del ${build_pkgs}
